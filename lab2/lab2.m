clear ; close all; clc
%% Metoda fzero opiera się na metodzie bisekcji
%% Argumenty funkcja, dwuelementowy przedział

p = [-pi/4, pi/4]
[X, FVAL, INFO, OUTPUT] = fzero(@(x) sin(x), p)
%% fzero(@sin, p)
p = sin([-pi/4, pi/4])
%%[X, FVAL, INFO, OUTPUT] = fzero(@cos, [0 2] - pi/2)
%% Tu niewłaściwe granice
%% [X, FVAL, INFO, OUTPUT] = fzero(@cos, [0, 111] - pi/2)

%%Fsolve
fsolve(@sin, [0 2] - pi/2)

%%Ustawienie prezycji
format long 

%%Implementujemy metodę newtona


function[x, y, iter] =  newton(f, df, x0, r, maxit=100, tol=1e-12)
    iter = 0;
    y = f(x0);
    x = x0 - y/df(x0);
    while abs(y) > tol && iter < maxit
        y = f(x);
        x0 = x;
        x = x -  y/df(x);
        e = (x-r) ./ (x0-r) .^ (1:3);
        iter = iter + 1;
        printf('iter=%d, e1=%f, e2=%f, e3=%f \n', iter, e(1), e(2), e(3));
            
    end
end

%%Drukowanie 
%%printf
%%disp
%%puts

[x,y,iter] = newton(@(x) x^2-2, @(x) 2*x, 10e12,  sqrt(2)/2)
[x,y,iter] = newton(@(x) (x-1)^4, @(x) 4*(x-1)^3, 2, 1)
[x,y,iter] = newton(@(x) cos(x), @(x) - sin(x), 1, pi/2)

%% w zależności od tego w której kolumnie nam się zeruje to taka zbieżność
