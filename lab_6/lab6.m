close all; clc

%% Wektory i wartości własne
function[v, l, k] = mpot(A, z0, maxit=1000, tol=1e-10)
    i = 0;
    z = z0;
    y = z / norm(z);
    sigma0 = 0;
    while i < maxit
        y = z / norm(z);
        z = A * y;
        sigma = y' * z;
        i = i + 1;
        if norm(sigma - sigma0) < tol * norm(sigma0)
            break
        endif
        sigma0 = sigma;
    end
    v = z;
    l = sigma;
    k = i;
end

%% Macierz symetryczna
n = 10;
A = round(unifrnd(0, 10, n, n));
A = (A' * A)';
[v, l, k] = mpot(A, unifrnd(0, 1, n, 1))
%% Sprawdzić czy OK

%% TODO QR

%% Interpolacja
%% a = polyfit(x, y, n)
%% x - węzły, y - wartości, n - stopień wielomianu
n = 60;
x = linspace(1, 10, n + 1);
y = log(x)
a = polyfit(x, y, n);
fx = polyval(a, x);
norm(y - fx)

t = linspace(1, 10, 100);
%% plot(t, log(t), t, polyval(a,t))



%% Przykład Rungego
n = 3;
l = -5
h = 5
x = linspace(l, h, n + 1);
y = 1 ./ (1 + x .^ 2);
a = polyfit(x, y, n);
fx = polyval(a, x);
norm(y - fx)

t = linspace(l, h, 100);
%% plot(t, 1 ./ (1 + t .^ 2), t, polyval(a,t))


%% żeby poprawić węzły Czebyszewa
i = 1:n+1;
x = ((l + h) / 2)  * ((h - l) / 2 * cos(((2 .* i - 1) / (2* (n + 1))) * pi)
a = polyfit(x, y, n);
t = linspace(l, h, 100);
plot(t, 1 ./ (1 + t .^ 2), t, polyval(a,t))


fx = polyval(a, x);
norm(y - fx)


