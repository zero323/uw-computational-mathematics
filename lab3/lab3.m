clear ; close all; clc

function y = f(x)
    y = (x - 1) .^ 4;
end

function y = g(x)
    y = x .^ 4 - 4 * (x .^ 3) + 6 * (x .^ 2) - (4 * x) + 1;
end

%% linspace(start, stop, [count])
x = linspace(1 - 3e-4, 1 + 3e-4, 1000);
plot(x, f(x));
hold on;
plot(x, g(x), col='4');


(4/3 - 1) * 3 - 1
(10.1 - 10) * 10 - 1
%% Przy mnożeniu przez potęgę 2 nie ma błędów, a 0.125 daje się
%% reprezentować
(10.125 - 10) * 8 - 1

%% Tu duże błędy
sqrt(x+1) - sqrt(x);
%% A tu nie
1 ./ (sqrt(x+1) + sqrt(x));

%% Układy równań liniowych
A = [1 2; 3 4];
b = [3; 7];

%% Droższa i mniej dokałdna metoda
x = inv(A) *b;
%% Szybsza, problem optymalizacji
x = A\b;

A = [1 1; 1 0.98];
b = [1; 2];
x = A\b

%% Różne normy
norm(A * x - b)
norm(A * x - b, 1)
norm(A * x -b, inf)

A = [1 1; 1 0.99];
b = [1; 2];
x = A\b
norm(A * x - b, 1)

A = [1 1; 1 1];
b = [1; 2];
x = A\b
norm(A * x - b, 1)

%% Macierz Hilberta
%% A = [a_{ij}], a_{ij} = 1 / (1 + j - 1) i,j = 1, ... ,n
%% Symetryczna, dodatnio określona
n = 100;
H = hilb(n);
xstar = ones(n,1);
b = H*xstar;
x1 =  H\b;
x2 = inv(H) * b;

%% Dla macierzy hilberta 
%% Zadanie jest bardzo trudne (źle uwarunkowanie) 
norm(x1 - xstar)
norm(x2 - xstar)

%% Możemy policzyć uwarunowanie
cond(hilb(n))

%% Problem sie bierzez macierzy odwrotnej
inv(hilb(n));

%% Odwortną możemy policzyć analitycznie,
%% ale wartosci b. duze
%% p_{i} = (-1)^i ( n + i - 1)  (n)
%%                ( i - 1    )  (i)
invhilb(5);

n = 100;
xstar = ones(n,1);
A = rand(n);
b = A * xstar;
x1 = A\b;
x2 = pinv(A) * b;
norm(x1 - xstar)
norm(x2 - xstar)
cond(A)


%% Rozkłąd na macierze LU
[L, U, P]  = lu(A)

%% Macierz trójdiagonalna
%% Żeby wyliczyć diagonalną górną
%% Musimy zerować tylko jeden element w każdym wierszu!!!
%% i jeden trzeba wyliczyć

%% Wtedy dla macierzy dolnej
%% stawiamy jedynki na diagonali
%% i współczynnik przez które dzieliliśmy dla górnej

%% Dostajemy macierze dwudiagonalne

%a_{2} = a_{2} - (c_{2} / a_{1}) * b_{1}




