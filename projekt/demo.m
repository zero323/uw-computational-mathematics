%% Maciej Szymkiewicz
clear ; close all; clc

%% Auxiliary functions
%% Compute error of QR decomposition (Householder)
function[err] = HouseholderQRError(A, H, R)
    ncols = size(H)(2);
    for i = 1:ncols
        v = H(:, ncols + 1 - i);
        gamma = v' * v / 2;
        for j = 1:ncols
            R(:, j) = R(:, j) - 1/gamma * v * v' * R(:, j);
        end
    end
    err = norm(A - R);
end

%% Compute error of QR decomposition (Gram-Schmidt)
function[err] = GSQRError(A, Q, R)
    ncols = size(Q)(2);
    err = norm(A - Q * R(1:ncols, :));
end

%% Compute orthogonalization error (GS)
function[err] = GSOrthogonalizationlError(Q)
    err = norm(Q' * Q - eye(size(Q)(2)));
end

%% Compute orthogonalization error (Householder)
%% Q'Q = I => Q'QX - X = zeros(size(X))
function[err] = HouseholderOrthogonalizationError(H)
    ncols = size(H)(2);
    nrows = size(H)(1);
    X = ones(nrows,1) ./ norm(ones(nrows,1));
    
    %% QX
    for i = 1:ncols
        v = H(:, ncols + 1 - i);
        gamma = v' * v / 2;
        for j = 1:ncols
            X = X - 1/gamma * v * v' * X;
        end
    end
    
    %% Q'QX
    for i = 1:ncols
        v = H(:, i);
        gamma = v' * v / 2;
        for j = 1:ncols
            X = X - 1/gamma * v * v' * X;
        end
    end

    err = norm(X - ones(nrows,1) ./ norm(ones(nrows,1)));
end

%% test function
function[A, b] = func(n, t, noise)
    v = unifrnd(0, 10, n, 1);
    A = [ones(n,1) v .^ 2 v ];
    if noise
        e = unifrnd(-1e-4, 1e-4, n, 1);
    else
        e = zeros(n,1);
    endif
    b =  t(1) * A(:,1) + t(2) * A(:,2) + t(3) * A(:,3) + e;
end


t = [2; 1; -5];

for n = [10, 20, 100]
    for noise = 0:1
        printf("---------------------------------\n")
        printf("Metoda Householdera, n = %i", n)
        if noise
            printf(" (zaburzone dane)")
        endif
        printf("\n")

        [A, b] = func(n, t, noise);
        [x, R, H] = Householder(A, b);
        qrerror = HouseholderQRError(A, H, R);
        printf("Błąd rozkładu QR:\t\t%e\n", qrerror)
        ortherror = HouseholderOrthogonalizationError(H);
        printf("Błąd ortogonalizacji:\t\t%e\n", ortherror)
        relative_solution_error = norm(t - x) / norm(t);
        printf("Względny błąd rozwiązania:\t%e\n", relative_solution_error)
        residual_error = norm(A*x - b);
        printf("Błąd rezydualny:\t\t%e\n", residual_error)

        printf("---------------------------------\n")
        printf("Ortogonalizacja GS, n = %i", n)
        if noise
            printf(" (zaburzone dane)")
        endif
        printf("\n")

        [x, R, Q] = GS(A, b);
        qrerror = GSQRError(A, Q, R);
        printf("Błąd rozkładu QR:\t\t%e\n", qrerror)
        ortherror = GSOrthogonalizationlError(Q);
        printf("Błąd ortogonalizacji:\t\t%e\n", ortherror)
        relative_solution_error = norm(x - t) / norm(x);
        printf("Względny błąd rozwiązania:\t%e\n", relative_solution_error)
        residual_error = norm(A*x - b);
        printf("Błąd rezydualny:\t\t%e\n", residual_error)

    end
end



