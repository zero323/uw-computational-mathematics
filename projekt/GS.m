%% Maciej Szymkiewicz

function[x, R, Q] = GS(A, b)
    R = zeros(size(A));
    Q = zeros(size(A));
    ncols = size(A)(2);

    R(1,1) = norm(A(:,1));
    Q(:,1) = 1/R(1,1) * A(1,1);

    for i = 1:ncols
        Q(:, i) = A(:, i);
        for j = 1:i-1
            Q(:,i) -= Q(:,j) * Q(:,j)' * A(:,i);
        end

        Q(:, i) = Q(:, i) / norm(Q(:, i));
        for j = i:ncols
            R(i, j) = Q(:, i)' * A(:,j);
        end
    end
    x = R(1:ncols, :) \ Q' * b;
end
