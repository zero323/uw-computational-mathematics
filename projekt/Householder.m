%% Maciej Szymkiewicz

function [x, R, H] = Householder(A, b)
    nrows = size(A)(1);
    ncols = size(A)(2);
    x = zeros(size(b));
    H = zeros(size(A));
    R = zeros(size(A));
    for i = 1:ncols
        %% Check sign of the element on diagonal
        s = ifelse(A(i,i) >= 0, 1, -1);
        %% Prepare vector vi (ui)
        v = [zeros(i - 1, 1); A(i:end, i)] + s * [zeros(i - 1, 1);  norm(A(i:end, i)); zeros(nrows - i, 1)];
        %% 2 ^ (-1) * ||v|| ^2
        gamma = v' * v / 2;
        %% Columns update
        for j = i:ncols
            A(i:end,j) = A(i:end,j) - 1/gamma * v(i:end) * v(i:end)' * A(i:end,j);
        end
        H(:,i) = v;
        b = b - 1/gamma * v * v' * b;
    end
    R = A;
    x = R(1:ncols,:) \ b(1:ncols);
end

