%% Matematyka obliczeniowa, laboratorium 1
%% Initialization
clear ; close all; clc

%% Deklarowanie wektorów i macierzy
puts("Deklarowanie wektorów i macierzy\n")
wektor_y = [1; 2; 3; 4; 5]
wektor_x = [-1 0 2 3 1]
macierz = [1 3 5; 7 1 9; 11 13 1]

%% Monożenie wektorów
wektor_x * wektor_y
wektor_y * wektor_x

%% Transpozycja
wektor_y'

%% Znajdowanie macierzy odwrotnej
pinv(macierz)

%% Operacje po współrzędnych (dodajemy kropkę)
wektor_y' .* wektor_x

%% Wykres
%% plot(1:10, randn(10,1))

%% Mnożenie przez siebie == podniesienie do kwadratu
macierz^2

%% Ogólnie funkcje wbudowane mogą być zastosowane do wektorów
sin(wektor_x)

%% Generowanie sekwencji start:step:stop, leniwa ewaluacja
1:10:100
(1:10:100)'
100:2:0

%% Składanie wektorów
[wektor_y', wektor_x]
[wektor_y'; wektor_x]
[wektor_y; wektor_x']

%% Indeksowanie wektorów
%% Proste przykłady
macierz
macierz(1, 3)
macierz(1:2, 2:3)
macierz(1,:)
macierz(end,:)

%% Można użyć wektora do wybierania elementów
macierz([1 2], [1 2])

%% Wybieranie wektorów o okreslonych właściwościach
n = randn(10,1)
n(n > 0)

%% Tworzenie specyficznych macierzy
zeros(3,3)
ones(3,3)
eye(10)

diag(macierz)
diag(wektor_x)

%% Pętle

i = 0
while i < 10
    i = i + 1;
end

puts(i)
puts('\n')

do
    i = i + 1;
until i < 20


puts(i)
puts('\n')

for k = 1:10
    i += 1;
end

puts(i)
puts('\n')



%% Porównanie czasu wykonania wektorowo i z pętlą
function out = sum_of_squares(n)
    tic
    x = 1:n;
    out = x * x';
    toc
end

function out = dummy_sum_of_squares(n)
    out = 0;
    tic
    for k = 1:n
        out += k*k;
    end
    toc
end

n = input("Podaj n")
n = 10000
sum_of_squares(n)
dummy_sum_of_squares(n)

%% Zwracanie kilku elementów

function [b,a] = zamien(a,b)
end

[a,b] = zamien(1,2)


%% Funkcje anonimowe
@(x) sin(x) + 2*x
