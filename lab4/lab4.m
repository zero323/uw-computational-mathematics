%% Zadanie to zerowanie macierzy trójdiagonalnej
%% b1 c1 0  0  0  0 
%% a2 b2 c2 0  0  0
%% 0  a3 b3 c3 0  0
%% 0  0  a4 b4 c4 0
%% 0  0  0  a5 b5 c5
%% clear ; close all; clc
zeros(1);

function [l, d] = lutri(a, b, c)
    l = zeros(size(a));
    d = zeros(size(b));
    d(1) = b(1);

    for i=2:length(b)
        l(i-1) = a(i - 1) / d(i - 1);
        d(i) = b(i) - c(i - 1) * l(i - 1);
    end

end

%% Dane wejściowe
n = 4;
a = -1 * ones(n - 1, 1);
b = 2 * ones(n, 1);
c = a;
[l, d] = lutri(a,b,c);

%% Sprawdzenie
L = eye(n) + diag(l,-1);
U = diag(d) + diag(a, 1);
L * U

%% diag(x, p) - tworzenie macierzy gdzie wektor na diagonali
n = 100
A = 3 * eye(n) + diag (ones(n-1,1), 1) + diag(ones(n-1,1), -1);

%% UWarunkowanie
cond(A);
chol(A);

%% Metofy iteracyjne
%% Metoda Jacobiego
%% Metoda Gaussa-Seidla

iter = 1000;

function[x, e] = jac(A, n, iter)
    xstar = ones(n,1);
    x = zeros(n,1);
    err = zeros(iter,1);

    b = A * xstar;
    D = diag(diag(A));

    for i = 1:iter
        x = D \ ((D - A) * x + b);
        err(i) = norm(x - xstar);
    end
    plot(err)
end

jac(A, n, iter);

function[x, e] = gauss_seidl(A, n, iter)
    xstar = ones(n,1);
    x = zeros(n,1);
    err = zeros(iter,1);

    for i = 1:iter
       x = tril(A) \ ((tril(A) -A) * x +b);
       err(i) = norm(x - xstar);
    end
    plot(err)
    
end

