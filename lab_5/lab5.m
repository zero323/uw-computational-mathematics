close all; clc
%% LZNK recznie
A = [2 1; 0 2; 2 4; 1 2]
b = [4; 4; 10; 5]
x = A \ b

%% Rozklad QR
[Q, R] = qr(A) 

%% Sprawdzamy czy jest ortogonalna
%% czyli, Q * Q' == I
norm(Q * Q' - eye(4))
norm(Q' * Q - eye(4))

%% Mamy wielomian y = x^2 - 5x + 2

function [y] =  f(x, a, b, c, emin, emax)
    y = a .* x.^2 .+ b .* x .+ c;
    %% Dodajemy błąd
    if emin != 0 && emax !=0
        y += unifrnd(emin, emax);
    endif
end

MIN = 0;
MAX = 6;
EMIN = -10e-3;
EMAX = 10e-3;
N = 20;
A = 1;
B = -5;
C = 2;

%% Losujemy X
X = unifrnd(MIN, MAX, N, 1);
%% Obliczamy wartości funkcji w X
Y = f(X, A, B, C, EMIN, EMAX);

P = [X.^2 X ones(N,1)] \ Y;

%% Sprawdzamy jaki błąd
function [P, A, E] = compute_error(X, Y, N, T)
   P = [X .^2 X ones(N ,1)] \ Y';
   A = [X .^2 X ones(N ,1)]
   E = norm(P - T);
end

%% Wykresy
t = linspace(0, 6, N);
Y = f(t, A, B, C, EMIN, EMAX);
[P, A] = compute_error(X, Y, N, [A; B; C])

plot(t, f(t, A, B, C, EMIN, EMAX), t, A * P )

%% Wektory własne 

A = [ 0 1 1; 1 0 1; 1 1 0];

[V, D] = eig(A)
%% Sprawdzenie
norm(A - V * D * V')
norm(A*V - V*D)


